#define CW_PIN 2   // CW (direction) nối với chân 2 của Arduino
#define CLK_PIN 3  // CLK (step) nối với chân 3 của Arduino
const int sensorPin = 7; // Chân cảm biến

// Các biến để điều chỉnh delay
int stepDelayUs = 500;  // Thời gian delay giữa các bước quay động cơ (microseconds)
int stopDelay = 3000; // Thời gian dừng động cơ khi phát hiện HIGH (ms)

void setup() {
  pinMode(CW_PIN, OUTPUT);
  pinMode(CLK_PIN, OUTPUT);
  pinMode(sensorPin, INPUT);
  Serial.begin(9600);

  digitalWrite(CW_PIN, HIGH); // Cố định chiều quay từ đầu
}

void loop() {
  int sensorValue = digitalRead(sensorPin); // Đọc giá trị từ cảm biến

  if (sensorValue == HIGH) {
    // Dừng động cơ khi cảm biến kích hoạt
    digitalWrite(CLK_PIN, LOW);
    delay(stopDelay); // Giữ động cơ dừng trong thời gian stopDelay
  } else {
    // Quay động cơ bước với delay microsecond để mượt hơn
    digitalWrite(CLK_PIN, HIGH);
    delayMicroseconds(stepDelayUs);
    digitalWrite(CLK_PIN, LOW);
    delayMicroseconds(stepDelayUs);
  }
}
